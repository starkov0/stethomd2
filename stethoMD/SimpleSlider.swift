//
//  RangeSlider.swift
//  CustomSliderExample
//
//  Created by William Archimede on 04/09/2014.
//  Copyright (c) 2014 HoodBrains. All rights reserved.
//

import UIKit
import QuartzCore

class SimpleSliderTrackLayer: CALayer {
    weak var simpleSlider: SimpleSlider?
    
    override func drawInContext(ctx: CGContext!) {
        if let slider = simpleSlider {
            // Clip
            let cornerRadius = bounds.height * slider.curvaceousness / 2.0
            let valuePosition = CGFloat(slider.positionForValue(slider.value))

            // highlighted part
            let highlightedRect = CGRect(x: bounds.origin.x, y: bounds.origin.y, width: valuePosition, height: bounds.height)
            let highlightedPath = UIBezierPath(roundedRect: highlightedRect, cornerRadius: cornerRadius)
            CGContextAddPath(ctx, highlightedPath.CGPath)
            CGContextSetFillColorWithColor(ctx, slider.trackHighlightTintColor.CGColor)
            CGContextAddPath(ctx, highlightedPath.CGPath)
            CGContextFillPath(ctx)
            
            // other part
            let rect = CGRect(x: valuePosition, y: bounds.origin.y, width: bounds.width - valuePosition, height: bounds.height)
            let path = UIBezierPath(roundedRect: rect, cornerRadius: cornerRadius)
            CGContextAddPath(ctx, path.CGPath)
            CGContextSetFillColorWithColor(ctx, slider.trackTintColor.CGColor)
            CGContextAddPath(ctx, path.CGPath)
            CGContextFillPath(ctx)
        }
    }
}

class SimpleSliderThumbLayer: CALayer {
    var highlighted: Bool = false {
        didSet {
            setNeedsDisplay()
        }
    }
    weak var simpleSlider: SimpleSlider?
    
    override func drawInContext(ctx: CGContext!) {
        if let slider = simpleSlider {
            let thumbFrame = bounds.rectByInsetting(dx: 1.0, dy: 1.0)
            let cornerRadius = thumbFrame.height * slider.curvaceousness / 2.0
            let thumbPath = UIBezierPath(roundedRect: thumbFrame, cornerRadius: cornerRadius)
            
            // Fill
            CGContextSetFillColorWithColor(ctx, slider.thumbTintColor.CGColor)
            CGContextAddPath(ctx, thumbPath.CGPath)
            CGContextFillPath(ctx)
            
            // Outline
            let strokeColor = UIColor.grayColor()
            CGContextSetStrokeColorWithColor(ctx, strokeColor.CGColor)
            CGContextSetLineWidth(ctx, 0.5)
            CGContextAddPath(ctx, thumbPath.CGPath)
            CGContextStrokePath(ctx)
            
            if highlighted {
                CGContextSetFillColorWithColor(ctx, UIColor(white: 0.0, alpha: 0.1).CGColor)
                CGContextAddPath(ctx, thumbPath.CGPath)
                CGContextFillPath(ctx)
            }
        }
    }
}

class SimpleSlider: UIControl {
    var mainView: UIView?
    
    var minimumValue: Double = 0.0 {
        willSet(newValue) {
            assert(newValue < maximumValue, "RangeSlider: minimumValue should be lower than maximumValue")
        }
        didSet {
            updateLayerFrames()
        }
    }
    
    var maximumValue: Double = 1.0 {
        willSet(newValue) {
            assert(newValue > minimumValue, "RangeSlider: maximumValue should be greater than minimumValue")
        }
        didSet {
            updateLayerFrames()
        }
    }
    
    var value: Double = 0.2 {
        didSet {
            updateLayerFrames()
        }
    }
    
    var trackHeight: CGFloat = 3.0 {
        didSet {
            updateLayerFrames()
        }
    }
    
    var gapBetweenThumbs: Double {
        return Double(thumbWidth)*(maximumValue - minimumValue) / Double(bounds.width)
    }
    
    var trackTintColor: UIColor = UIColor(white: 0.9, alpha: 1.0) {
        didSet {
            trackLayer.setNeedsDisplay()
        }
    }
    
    var trackHighlightTintColor: UIColor = UIColor(red: 0.0, green: 0.45, blue: 0.94, alpha: 1.0) {
        didSet {
            trackLayer.setNeedsDisplay()
        }
    }
    
    var thumbTintColor: UIColor = UIColor.whiteColor() {
        didSet {
            thumbLayer.setNeedsDisplay()
        }
    }
    
    var curvaceousness: CGFloat = 1.0 {
        didSet(newValue) {
            if newValue < 0.0 {
                curvaceousness = 0.0
            }
            
            if newValue > 1.0 {
                curvaceousness = 1.0
            }
            
            trackLayer.setNeedsDisplay()
            thumbLayer.setNeedsDisplay()
        }
    }
    
    private var previouslocation = CGPoint()
    
    private let trackLayer = SimpleSliderTrackLayer()
    private let thumbLayer = SimpleSliderThumbLayer()
    
    private var thumbWidth: CGFloat {
        return CGFloat(bounds.height)
    }
    
    override var frame: CGRect {
        didSet {
            updateLayerFrames()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        trackLayer.simpleSlider = self
        trackLayer.contentsScale = UIScreen.mainScreen().scale
        layer.addSublayer(trackLayer)
        
        thumbLayer.simpleSlider = self
        thumbLayer.contentsScale = UIScreen.mainScreen().scale
        layer.addSublayer(thumbLayer)
        
        updateLayerFrames()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func updateLayerFrames() {
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        
        trackLayer.frame = CGRect(x: 0, y: trackHeight/2, width: bounds.width, height: trackHeight)
        trackLayer.setNeedsDisplay()
        
        let thumbCenter = CGFloat(positionForValue(value))
        thumbLayer.frame = CGRect(
            x: thumbCenter - thumbWidth/2,
            y: trackLayer.frame.origin.y + trackLayer.frame.height/2 - thumbWidth/2,
            width: thumbWidth,
            height: thumbWidth)
        thumbLayer.setNeedsDisplay()
        
        CATransaction.commit()
    }
    
    func positionForValue(value: Double) -> Double {
        return Double(bounds.width - thumbWidth) * (value - minimumValue) /
            (maximumValue - minimumValue) + Double(thumbWidth / 2.0)
    }
    
    func boundValue(value: Double, toLowerValue lowerValue: Double, upperValue: Double) -> Double {
        return min(max(value, lowerValue), upperValue)
    }

    // MARK: - Touches
    override func beginTrackingWithTouch(touch: UITouch, withEvent event: UIEvent) -> Bool {
        previouslocation = touch.locationInView(self)

        // Hit test the thumb layers
        if thumbLayer.frame.contains(previouslocation) {
            thumbLayer.highlighted = true
        }
        
        return thumbLayer.highlighted
    }
    
    override func continueTrackingWithTouch(touch: UITouch, withEvent event: UIEvent) -> Bool {
        let location = touch.locationInView(self)
        
        // Determine by how much the user has dragged
        let deltaLocation = Double(location.x - previouslocation.x)
        let deltaValue = (maximumValue - minimumValue) * deltaLocation / Double(bounds.width - bounds.height)

        previouslocation = location
        
        // Update the values
        if thumbLayer.highlighted {
            value = boundValue(value + deltaValue, toLowerValue: minimumValue, upperValue: maximumValue)
        }
        
        sendActionsForControlEvents(.ValueChanged)
        
        return true
    }
    
    override func endTrackingWithTouch(touch: UITouch, withEvent event: UIEvent) {
        thumbLayer.highlighted = false
    }
}
