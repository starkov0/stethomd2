//
//  GraphMode.swift
//  stethoMD
//
//  Created by Pierre Starkov on 30/04/15.
//  Copyright (c) 2015 Pierre Starkov. All rights reserved.
//

import Foundation

enum ModeEnum {
    case FFT
    case Time
}