//
//  GraphContainer.swift
//  stethoMD
//
//  Created by Pierre Starkov on 15/06/15.
//  Copyright (c) 2015 Pierre Starkov. All rights reserved.
//

import Foundation

class ModeContainer: NSObject {
    static let sharedInstance = ModeContainer()
    
    var _graphMode: ModeEnum = ModeEnum.Time
    
    func setGraphMode(graphMode: ModeEnum) {
        _graphMode = graphMode
    }
    func getGraphMode() -> ModeEnum {
        return _graphMode
    }
    
}