//
//  GraphLabelView.swift
//  stethoMD
//
//  Created by Pierre Starkov on 30/04/15.
//  Copyright (c) 2015 Pierre Starkov. All rights reserved.
//

import Foundation
import UIKit

class LabelView: UIView {
    let _axisContainer = AxisContainer.sharedInstance
    let _labelContainer = LabelContainer.sharedInstance
    let _myLinspace = MyLinspace.sharedInstance
    
    func drawLabelX() {
        var linspaceArray = _myLinspace.linspace(
            Int(_axisContainer.getX()),
            end: Int(_axisContainer.getWidth()),
            count: _labelContainer.getLabelX().count)
        
        for var i=0; i<linspaceArray.count; i++ {
            var label = UILabel(
                frame: CGRectMake(
                    CGFloat(linspaceArray[i]),
                    _axisContainer.getHeight() + _axisContainer.getY(),
                    _axisContainer.getPadSize(),
                    _axisContainer.getPadSize()))
            label.font = UIFont(name: "Futura", size: 10.0)
            label.textAlignment = NSTextAlignment.Center
            label.text = "\(_labelContainer.getLabelX()[i])"
            self.addSubview(label)
        }
    }
    
    func drawLabelY() {
        var positiveLinspace = _myLinspace.linspace(
            Int(_axisContainer.getHeight()/2 + _axisContainer.getY()/2),
            end: Int(_axisContainer.getY()),
            count: _labelContainer.getLabelY().count)
        
        var negativeLinspace = _myLinspace.linspace(
            Int(_axisContainer.getHeight()/2 + _axisContainer.getY()/2),
            end: Int(_axisContainer.getHeight()),
            count: _labelContainer.getLabelY().count)
        
        for var i=0; i<positiveLinspace.count; i++ {
            // positive label
            var positiveLabel = UILabel(
                frame: CGRectMake(
                    CGFloat(0),
                    CGFloat(positiveLinspace[i]),
                    _axisContainer.getPadSize(),
                    _axisContainer.getPadSize()))
            positiveLabel.font = UIFont(name: "Futura", size: 10.0)
            positiveLabel.textAlignment = NSTextAlignment.Center
            positiveLabel.text = "\(_labelContainer.getLabelY()[i])"
            self.addSubview(positiveLabel)
            
            // negative label
            var negativeLabel = UILabel(
                frame: CGRectMake(
                    CGFloat(0),
                    CGFloat(negativeLinspace[i]),
                    _axisContainer.getPadSize(),
                    _axisContainer.getPadSize()))
            negativeLabel.font = UIFont(name: "Futura", size: 10.0)
            negativeLabel.textAlignment = NSTextAlignment.Center
            negativeLabel.text = "\(-_labelContainer.getLabelY()[i])"
            self.addSubview(negativeLabel)
        }
    }
    
    func removeLabels() {
        // get views
        var labelsToRemove = [UIView]()
        for var i=0; i<self.subviews.count; i++ {
            var subView = self.subviews[i] as! UIView
            labelsToRemove.append(subView)
        }
        for var i=0; i<labelsToRemove.count; i++ {
            labelsToRemove[i].removeFromSuperview()
        }
    }
}