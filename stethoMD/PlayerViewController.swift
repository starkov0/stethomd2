//
//  PlayViewController.swift
//  stethoMD
//
//  Created by Pierre Starkov on 29/06/15.
//  Copyright (c) 2015 Pierre Starkov. All rights reserved.
//

import Foundation
import UIKit
import MediaPlayer

class PlayerViewController: UIViewController {
    var _graphViewController = GraphViewController.sharedInstance
    var _labelViewController = LabelViewController.sharedInstance
    var _axisViewController = AxisViewController.sharedInstance
    var _rangeSliderController = RangeSliderController.sharedInstance
    
    var _rangeSlider = RangeSlider(frame: CGRectZero)
    var _myLinspace = MyLinspace.sharedInstance
    var _recordController = RecordController.sharedInstance
    
    var _audioContainer = AudioContainer.sharedInstance
    var _axisContainer = AxisContainer.sharedInstance
    var _labelContainer = LabelContainer.sharedInstance
    var _modeContainer = ModeContainer.sharedInstance
    
    var _isPlaying: Bool = false
    
    @IBOutlet weak var _graphView: GraphView!
    @IBOutlet weak var _axisView: AxisView!
    @IBOutlet weak var _labelView: LabelView!
    
    @IBOutlet weak var _routingIOView: UIView!
    @IBOutlet weak var _rangeSliderView: UIView!
    
    @IBOutlet weak var _modeButton: UIButton!
    @IBOutlet weak var _ppButton: UIButton!
    @IBOutlet weak var _stopButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // volumeView
        var volumeView = MPVolumeView(frame: _routingIOView.bounds)
        volumeView.backgroundColor = UIColor.blueColor()
        volumeView.showsRouteButton = true
        volumeView.showsVolumeSlider = false
        _routingIOView.addSubview(volumeView)
        
        // buttons
        _modeButton.setTitle("TIME", forState: UIControlState.Normal)
        _ppButton.setTitle("PLAY", forState: UIControlState.Normal)
        _stopButton.setTitle("STOP", forState: UIControlState.Normal)
    }

    func startPlaying() {
        // player
        _graphViewController.startPlayer()
        
        // text
        navigationController?.navigationBar.topItem?.title = "PLAYING"
        _ppButton.setTitle("PAUSE", forState: UIControlState.Normal)
        
        // bool
        _isPlaying = true
    }
    
    func pausePlaying() {
        // player
        _graphViewController.pausePlayer()
        
        // text
        navigationController?.navigationBar.topItem?.title = "PAUSE"
        _ppButton.setTitle("PLAY", forState: UIControlState.Normal)
        
        // bool
        _isPlaying = false
    }
    
    func stopPlaying() {
        // player
        _graphViewController.stopPlayer()
        
        // text
        navigationController?.navigationBar.topItem?.title = "STOP"
        _ppButton.setTitle("PLAY", forState: UIControlState.Normal)
    
        // bool
        _isPlaying = false
    }
    
    @IBAction func ppButtonPressed(sender: AnyObject) {
        if _isPlaying {
            self.pausePlaying()
        } else {
            self.startPlaying()
        }
    }
    
    @IBAction func stopButtonPressed(sender: AnyObject) {
        self.stopPlaying()
    }
    
    @IBAction func modeButtonPressed(sender: AnyObject) {
        if _modeContainer.getGraphMode() == ModeEnum.Time {
            // mode
            _modeContainer.setGraphMode(ModeEnum.FFT)
            
            // text
            _modeButton.setTitle("FFT", forState: UIControlState.Normal)
            
            // container
            _audioContainer.setAudioSize(Int(_axisContainer.getWidth()))
        } else if _modeContainer.getGraphMode() == ModeEnum.FFT {
            // mode
            _modeContainer.setGraphMode(ModeEnum.Time)
            
            // text
            _modeButton.setTitle("TIME", forState: UIControlState.Normal)
            
            // container
            _audioContainer.setAudioSize(Int(_axisContainer.getWidth()))
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        // container
        _audioContainer.setAudioSize(Int(_axisContainer.getWidth()))
        
        // axis
        _axisViewController.setView(_axisView)
        _axisViewController.display()
        
        // label
        _labelViewController.setView(_labelView)
        _labelViewController.display()
        
        // graph
        _graphViewController.setView(_graphView)
        
        // range slider
        view.addSubview(_rangeSlider)
        _rangeSlider.frame = _rangeSliderView.frame
        _rangeSliderController.setRangeSlider(_rangeSlider)
        
        // graph view
        _graphViewController.connectPlayerNode()
        self.startPlaying()
    }
    
    override func viewWillDisappear(animated:Bool) {
        self.stopPlaying()
        _graphViewController.disconnectPlayerNode()
    }

}