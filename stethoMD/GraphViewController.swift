//
//  GraphController.swift
//  stethoMD
//
//  Created by Pierre Starkov on 13/04/15.
//  Copyright (c) 2015 Pierre Starkov. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

class GraphViewController: NSObject {
    static let sharedInstance = GraphViewController()
    
    var _graphView: GraphView!
    var _audioController = AudioController.sharedInstance
    var _frequencyContainer = FrequencyContainer.sharedInstance
    var _engine: AVAudioEngine = AVAudioEngine()
    var _eqNode: AVAudioUnitEQ = AVAudioUnitEQ(numberOfBands: 2)
    var _audioFormat: AVAudioFormat!
    var _audioFile: AVAudioFile!
    var _player: AVAudioPlayerNode = AVAudioPlayerNode()
    var _audioFileController = AudioFilesController.sharedInstance


    override init() {
        
        // init
        super.init()
        
        // audioFormat
        _audioFormat = AVAudioFormat(
            standardFormatWithSampleRate: _engine.inputNode.inputFormatForBus(0).sampleRate,
            channels: _engine.inputNode.inputFormatForBus(0).channelCount
        )
        
        // eqNode
        _eqNode.globalGain = 1
        _engine.attachNode(_eqNode)
        
        var filterParams0 = _eqNode.bands[0] as! AVAudioUnitEQFilterParameters
        filterParams0.frequency = Float(_frequencyContainer.getLowerFrequency())
        filterParams0.filterType = AVAudioUnitEQFilterType.HighPass
        filterParams0.bypass = false
        
        var filterParams1 = _eqNode.bands[1] as! AVAudioUnitEQFilterParameters
        filterParams1.frequency = Float(_frequencyContainer.getUpperFrequency())
        filterParams1.filterType = AVAudioUnitEQFilterType.LowPass
        filterParams1.bypass = false
        
        _engine.connect(_eqNode, to: _engine.outputNode, format: _audioFormat)
        
        // player
        _engine.attachNode(_player)

        // tap
        _engine.outputNode.installTapOnBus(0, bufferSize: 1024, format: nil) {
            (buffer: AVAudioPCMBuffer!, time: AVAudioTime!) in
            
            // set buffer length
            buffer.frameLength = AVAudioFrameCount(self._engine.inputNode.inputFormatForBus(0).sampleRate / 10)
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                // get audio
                var audio = Array<Float>()
                var channelCount = Int(self._engine.inputNode.outputFormatForBus(0).channelCount)
                for var i=0; i<Int(buffer.frameLength)*channelCount; i+=channelCount {
                    audio.append(buffer.floatChannelData[0][i])
                }
                
                // process audio
                self._audioController.processAudio(audio)
                
                // display audio
                dispatch_async(dispatch_get_main_queue(), {
                    self._graphView.setNeedsDisplay()
                })
            })
        }
    }
    
    // view
    func setView(graphView: GraphView) {
        _graphView = graphView
    }
    
    // frequencies
    func setFrequencies(lowerValue: Double, upperValue: Double) {
        var filterParams0 = _eqNode.bands[0] as! AVAudioUnitEQFilterParameters
        var filterParams1 = _eqNode.bands[1] as! AVAudioUnitEQFilterParameters
        filterParams0.frequency = Float(lowerValue)
        filterParams1.frequency = Float(upperValue)
    }
    
    // connect input
    func connectReaderNode() {
        _engine.connect(_engine.inputNode, to: _eqNode, format: _audioFormat)
    }
    
    func connectPlayerNode() {
        _engine.connect(_player, to: _eqNode, format: _audioFormat)
        
        var url = NSURL(fileURLWithPath: _audioFileController.getCurrentFile())
        _audioFile = AVAudioFile(forReading: url, error: nil)
    }
    
    // disconnect input
    func disconnectReaderNode() {
        _engine.disconnectNodeInput(_engine.inputNode)
    }
    
    
    func disconnectPlayerNode() {
        _engine.disconnectNodeInput(_player)
    }
    
    // start
    func startReader() {
        self.startEngine()
    }
    
    func startPlayer() {
        _player.scheduleFile(_audioFile, atTime: nil, completionHandler: nil)
        self.startEngine()
        _player.play()
    }
    
    // stop
    func stopReader() {
        self.stopEngine()
    }
    
    func stopPlayer() {
        _player.stop()
        self.stopEngine()
    }
    
    // pause
    func pausePlayer() {
        _player.pause()
        self.stopEngine()
    }
    
    private func startEngine() {
        _engine.prepare()
        if !_engine.startAndReturnError(nil) {
            println("startEngineError")
        }
    }
    
    private func stopEngine() {
        _engine.stop()
    }
    
    func getSampleRate() -> Double {
        return _engine.inputNode.inputFormatForBus(0).sampleRate
    }

}




