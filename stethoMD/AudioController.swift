//
//  AudioController.swift
//  stethoMD
//
//  Created by Pierre Starkov on 25/06/15.
//  Copyright (c) 2015 Pierre Starkov. All rights reserved.
//

import Foundation

class AudioController: NSObject {
    static let sharedInstance = AudioController()
    
    var _audioContainer = AudioContainer.sharedInstance
    var _myLinspace = MyLinspace.sharedInstance
    var _myFFT = MyFFT.sharedInstance
    var _graphContainer = ModeContainer.sharedInstance
    var _axisContainer = AxisContainer.sharedInstance
    
    func processAudio (audio: [Float]) {
        if _graphContainer.getGraphMode() == ModeEnum.Time {
            var subAudio = _myLinspace.linspaceAudio(
                audio,
                size: Int(_axisContainer.getWidth())/10
            )
            
            for var i=0; i<subAudio.count; i++ {
                subAudio[i] = subAudio[i]*3
            }
            
            _audioContainer.appendAudio(subAudio)
        }
        else if _graphContainer.getGraphMode() == ModeEnum.FFT {
            var fftAudio = _myFFT.fft(audio)
            var subAudio = _myLinspace.logspaceAudio(
                fftAudio,
                size: Int(_axisContainer.getWidth())
            )
            
            for var i=0; i<subAudio.count; i++ {
                subAudio[i] = subAudio[i]*10
            }
            
            _audioContainer.setAudio(subAudio)
        }
    }

}