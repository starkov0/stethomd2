//
//  AxisViewController.swift
//  stethoMD
//
//  Created by Pierre Starkov on 25/06/15.
//  Copyright (c) 2015 Pierre Starkov. All rights reserved.
//

import Foundation
import UIKit

class AxisViewController: NSObject {
    static let sharedInstance = AxisViewController()

    var _axisView: AxisView!

    func setView(axisView: AxisView) {
        _axisView = axisView
    }
    
    func display() {
        _axisView.setNeedsDisplay()
    }
    
}