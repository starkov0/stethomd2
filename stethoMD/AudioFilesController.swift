//
//  RecordingContainer.swift
//  stethoMD
//
//  Created by Pierre Starkov on 28/06/15.
//  Copyright (c) 2015 Pierre Starkov. All rights reserved.
//

import Foundation

class AudioFilesController: NSObject {
    static let sharedInstance = AudioFilesController()

    var _currentFile: String!
    var _myRegex = MyRegex.sharedInstance
    var _filemanager = NSFileManager.defaultManager()
    var _documentsPath = NSHomeDirectory() + "/Documents/"
    var _fileLabel = "StethoMD@"

    func removeAudioFile(fileName: String) {
        var path = _documentsPath + fileName
        var error: NSError?
        if !_filemanager.removeItemAtPath(path, error: &error) {
            println("Failed to delete directory: \(error!.localizedDescription)")
        }
    }
    
    func queryAudioFiles() -> [String] {
        var _audioFiles = [String]()
        
        let files = _filemanager.enumeratorAtPath(_documentsPath)
        while let file: AnyObject = files?.nextObject() {
            if _myRegex.matchesForRegexInText(_fileLabel, text: file as! String) {
                _audioFiles.append(file as! String)
            }
        }

        return _audioFiles
    }
    
    func createNewSoundFilePath() -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy@HH:mm:ss";
        var fileName =  _fileLabel + dateFormatter.stringFromDate(NSDate()) + ".m4a"
        return _documentsPath.stringByAppendingPathComponent(fileName)
    }
    
    func setCurrentFile(fileName: String) {
        _currentFile = _documentsPath + fileName
    }
    
    func getCurrentFile() -> String {
        return _currentFile
    }
}