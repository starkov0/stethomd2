//
//  MainViewController.swift
//  mobileMD
//
//  Created by Pierre Starkov on 09/04/15.
//  Copyright (c) 2015 Pierre Starkov. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import MediaPlayer

class RecorderViewController: UIViewController {
    var _graphViewController = GraphViewController.sharedInstance
    var _labelViewController = LabelViewController.sharedInstance
    var _axisViewController = AxisViewController.sharedInstance
    var _rangeSliderController = RangeSliderController.sharedInstance
    
    var _rangeSlider = RangeSlider.sharedInstance
    var _myLinspace = MyLinspace.sharedInstance
    var _recordController = RecordController.sharedInstance
    
    var _audioContainer = AudioContainer.sharedInstance
    var _axisContainer = AxisContainer.sharedInstance
    var _labelContainer = LabelContainer.sharedInstance
    var _modeContainer = ModeContainer.sharedInstance
    
    @IBOutlet weak var _graphView: GraphView!
    @IBOutlet weak var _axisView: AxisView!
    @IBOutlet weak var _labelView: LabelView!
    
    @IBOutlet weak var _routingIOView: UIView!
    @IBOutlet weak var _rangeSliderView: UIView!
    
    @IBOutlet weak var _readButton: UIButton!
    @IBOutlet weak var _modeButton: UIButton!
    @IBOutlet weak var _recordButton: UIButton!
    
    var _isReading: Bool = false
    
    // app init function
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // enable bluetooth
        if !AVAudioSession.sharedInstance().setActive(false, error: nil) {
            println("deactivationError")
        }
        if !AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord,
            withOptions: AVAudioSessionCategoryOptions.AllowBluetooth,
            error: nil) {
            println("setCategoryError")
        }
        if !AVAudioSession.sharedInstance().setActive(true, error: nil) {
            println("activationError")
        }
        
        // containers
        _axisContainer.setBounds(_axisView.getBounds())
        _labelContainer.setLabelX(_myLinspace.linspace(20, end: 5000, count: 10))
        _labelContainer.setLabelY(_myLinspace.linspace(0, end: 100, count: 5))
        _modeContainer.setGraphMode(ModeEnum.Time)

        // volumeView
        var volumeView = MPVolumeView(frame: _routingIOView.bounds)
        volumeView.backgroundColor = UIColor.blueColor()
        volumeView.showsRouteButton = true
        volumeView.showsVolumeSlider = false
        _routingIOView.addSubview(volumeView)
        
        // buttons
        _readButton.setTitle("READ", forState: UIControlState.Normal)
        _modeButton.setTitle("TIME", forState: UIControlState.Normal)
        _recordButton.setTitle("RECORD", forState: UIControlState.Normal)
        
        // range slider
        _rangeSliderController.setGraphViewController(_graphViewController)
    }
    
    func startReading() {
        // read
        _graphViewController.startReader()
        
        // text
        navigationController?.navigationBar.topItem?.title = "READING"
        _readButton.setTitle("STOP", forState: UIControlState.Normal)
        _recordButton.enabled = true
        
        // bool
        _isReading = true
    }

    func stopReading() {
        // record
        if _recordController.isRecording() {
            self.stopRecording()
        }

        // read
        _graphViewController.stopReader()
    
        // text
        _recordButton.enabled = false
        navigationController?.navigationBar.topItem?.title = "STOP"
        _readButton.setTitle("READ", forState: UIControlState.Normal)
        
        // bool
        _isReading = false
    }

    func startRecording() {
        // record
        _recordController.start()
        
        // text
        navigationController?.navigationBar.topItem?.title = "RECORDING"
        _recordButton.setTitle("STOP", forState: UIControlState.Normal)
    }
    
    func stopRecording() {
        // record
        _recordController.stop()
        
        // text
        navigationController?.navigationBar.topItem?.title = "READING"
        _recordButton.setTitle("RECORD", forState: UIControlState.Normal)
    }
    
    @IBAction func readButtonPressed() {
        if _isReading {
            self.stopReading()
        } else {
            self.startReading()
        }
    }

    @IBAction func recordButtonPressed() {
        if _recordController.isRecording() {
            self.stopRecording()
        } else {
            self.startRecording()
        }
    }

    @IBAction func modeButtonPressed() {
        if _modeContainer.getGraphMode() == ModeEnum.Time {
            // mode
            _modeContainer.setGraphMode(ModeEnum.FFT)

            // text
            _modeButton.setTitle("FFT", forState: UIControlState.Normal)
        
            // container
            _audioContainer.setAudioSize(Int(_axisContainer.getWidth()))
        } else if _modeContainer.getGraphMode() == ModeEnum.FFT {
            // mode
            _modeContainer.setGraphMode(ModeEnum.Time)
            
            // text
            _modeButton.setTitle("TIME", forState: UIControlState.Normal)
        
            // container
            _audioContainer.setAudioSize(Int(_axisContainer.getWidth()))
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        // container
        _audioContainer.setAudioSize(Int(_axisContainer.getWidth()))
        
        // axis
        _axisViewController.setView(_axisView)
        _axisViewController.display()
        
        // label
        _labelViewController.setView(_labelView)
        _labelViewController.display()
        
        // graph
        _graphViewController.setView(_graphView)
        
        // range slider
        view.addSubview(_rangeSlider)
        _rangeSlider.frame = _rangeSliderView.frame
        _rangeSliderController.setRangeSlider(_rangeSlider)
        
        // graph view
        _graphViewController.connectReaderNode()
        self.startReading()
    }
    
    override func viewWillDisappear(animated:Bool) {
        self.stopReading()
        _graphViewController.disconnectReaderNode()
    }
}


