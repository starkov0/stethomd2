//
//  RecordController.swift
//  stethoMD
//
//  Created by Pierre Starkov on 27/06/15.
//  Copyright (c) 2015 Pierre Starkov. All rights reserved.
//

import Foundation
import AVFoundation

class RecordController: NSObject {
    static let sharedInstance = RecordController()
    
    var _recorder: AVAudioRecorder!
    var _isRecording = false
    var _audioFilesController = AudioFilesController.sharedInstance
    var _graphViewController = GraphViewController.sharedInstance
    
    func start() {
        var settings: [NSObject: AnyObject] = [
            AVFormatIDKey: kAudioFormatAppleLossless,
            AVEncoderAudioQualityKey : AVAudioQuality.Max.rawValue,
            AVEncoderBitRateKey : 320000,
            AVNumberOfChannelsKey: 2,
            AVSampleRateKey : _graphViewController.getSampleRate()
        ]
        
        var error: NSError?
        
        _recorder = AVAudioRecorder(
            URL: NSURL(fileURLWithPath: _audioFilesController.createNewSoundFilePath()),
            settings: settings,
            error: &error
        )
        
        if let err = error {
            println("audioSession error: \(err.localizedDescription)")
        } else {
            _recorder.prepareToRecord()
        }
        _recorder.record()
        _isRecording = true
    }
    
    func stop() {
        _recorder.stop()
        _isRecording = false
    }
    
    func isRecording() -> Bool {
        return _isRecording
    }
}