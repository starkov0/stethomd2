//: Playground - noun: a place where people can play

import Cocoa

func matchesForRegexInText(regex: String!, text: String!) -> Bool {
    let regex = NSRegularExpression(pattern: regex,
        options: nil, error: nil)!
    let nsString = text as NSString
    let results = regex.matchesInString(text,
        options: nil,
        range: NSMakeRange(0, nsString.length))as! [NSTextCheckingResult]
    return results.count > 0
}

let m = matchesForRegexInText("stethoMD-", "asdstethoMD-28-06-2015@07:50:49.m4a")
println(m)

let filemanager = NSFileManager.defaultManager()
let files = filemanager.enumeratorAtPath(NSHomeDirectory() + "/Documents")
var a = filemanager.contentsAtPath(NSHomeDirectory() + "/Documents")
