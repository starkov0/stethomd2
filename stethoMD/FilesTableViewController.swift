//
//  RecordingViewController.swift
//  stethoMD
//
//  Created by Pierre Starkov on 03/05/15.
//  Copyright (c) 2015 Pierre Starkov. All rights reserved.
//

import Foundation
import UIKit

class FilesTableViewController: UITableViewController, UITableViewDataSource, UITableViewDelegate {
    
    var _audioFilesController = AudioFilesController.sharedInstance
    var _names: [String]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tableView = self.view as! UITableView
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44.0
    }
    
    override func viewWillAppear(animated: Bool) {
        _names = _audioFilesController.queryAudioFiles()
    }
    
    // CREATE
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _names.count
    }
    
    // CREATE
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! UITableViewCell
        cell.textLabel?.text = _names[indexPath.row]
        return cell
    }
    
    // DELETE
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    // DELETE
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            var selectedCell = tableView.cellForRowAtIndexPath(indexPath)
            _audioFilesController.removeAudioFile(selectedCell!.textLabel!.text!)
            _names = _audioFilesController.queryAudioFiles()
            tableView.reloadData()
        }
    }
    
    // OPEN
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var selectedCell = tableView.cellForRowAtIndexPath(indexPath)
        _audioFilesController.setCurrentFile(selectedCell!.textLabel!.text!)
    }
    
}