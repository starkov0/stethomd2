//
//  LabelViewController.swift
//  stethoMD
//
//  Created by Pierre Starkov on 25/06/15.
//  Copyright (c) 2015 Pierre Starkov. All rights reserved.
//

import Foundation
import UIKit

class LabelViewController: NSObject {
    static let sharedInstance = LabelViewController()
    
    var _labelView: LabelView!
    var _modeContainer = ModeContainer.sharedInstance
    
    func setView(labelView: LabelView) {
        _labelView = labelView
    }
    
    func display() {
        _labelView.removeLabels()
        _labelView.drawLabelX()
        _labelView.drawLabelY()
    }
    
}