//
//  FFT.swift
//  stethoMD
//
//  Created by Pierre Starkov on 15/06/15.
//  Copyright (c) 2015 Pierre Starkov. All rights reserved.
//

import Foundation
import Accelerate

class MyFFT : NSObject {
    static let sharedInstance = MyFFT()
    
    func sqrt(x: [Float]) -> [Float] {
        var results = [Float](count: x.count, repeatedValue: 0.0)
        vvsqrtf(&results, x, [Int32(x.count)])
        return results
    }
    func fft(input: [Float]) -> [Float] {
        // fft computation
        var real = [Float](input)
        var imaginary = [Float](count: input.count, repeatedValue: 0.0)
        var splitComplex = DSPSplitComplex(realp: &real, imagp: &imaginary)
        let length = vDSP_Length(floor(log2(Float(input.count))))
        let radix = FFTRadix(kFFTRadix2)
        let weights = vDSP_create_fftsetup(length, radix)
        vDSP_fft_zip(weights, &splitComplex, 1, length, FFTDirection(FFT_FORWARD))
        var magnitudes = [Float](count: input.count, repeatedValue: 0.0)
        vDSP_zvmags(&splitComplex, 1, &magnitudes, 1, vDSP_Length(input.count))
        var normalizedMagnitudes = [Float](count: input.count, repeatedValue: 0.0)
        vDSP_vsmul(sqrt(magnitudes), 1, [2.0 / Float(input.count)], &normalizedMagnitudes, 1, vDSP_Length(input.count))
        vDSP_destroy_fftsetup(weights)
        
        // return half of the fft
        var fftArray = [Float]()
        for var i=0; i<normalizedMagnitudes.count/2; i++ {
            fftArray.append(normalizedMagnitudes[i])
        }
        
        return fftArray
    }
}