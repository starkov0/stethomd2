//
//  GraphContainer.swift
//  stethoMD
//
//  Created by Pierre Starkov on 13/04/15.
//  Copyright (c) 2015 Pierre Starkov. All rights reserved.
//

import Foundation
import UIKit

class AudioContainer: NSObject {
    static let sharedInstance = AudioContainer()
    
    var _audio: [Float] = [Float]()
    
    func setAudioSize(audioSize: Int) {
        _audio.removeAll(keepCapacity: false)
        for var i=0; i<audioSize; i++ {
            _audio.append(0.0)
        }
    }
    
    func appendAudio(audio: Array<Float>) {
        for var i=0; i<audio.count; i++ {
            _audio.removeAtIndex(0)
            _audio.append(audio[i])
        }
    }
    
    func setAudio(audio: Array<Float>) {
        _audio = audio
    }
    
    func getAudio() -> [Float] {
        return _audio
    }
}