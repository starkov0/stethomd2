//
//  SliderViewController.swift
//  stethoMD
//
//  Created by Pierre Starkov on 25/06/15.
//  Copyright (c) 2015 Pierre Starkov. All rights reserved.
//

import Foundation
import UIKit

class RangeSliderController: NSObject {
    static let sharedInstance = RangeSliderController()
    
    var _frequencyContainer = FrequencyContainer.sharedInstance
    var _graphViewController: GraphViewController!
    var _rangeSlider: RangeSlider!
    
    func setGraphViewController(graphViewController: GraphViewController) {
        _graphViewController = graphViewController
    }
    
    func setRangeSlider(rangeSlider: RangeSlider) {
        _rangeSlider = rangeSlider
        _rangeSlider.maximumValue = _frequencyContainer.getMaxFrequency()
        _rangeSlider.minimumValue = _frequencyContainer.getMinFrequency()
        _rangeSlider.upperValue = _frequencyContainer.getMaxFrequency()
        _rangeSlider.lowerValue = _frequencyContainer.getMinFrequency()
        _rangeSlider.addTarget(self,
            action: "rangeSliderValueChanged:",
            forControlEvents: UIControlEvents.ValueChanged)
    }
    
    func rangeSliderValueChanged(rangeSlider: RangeSlider) {
        _frequencyContainer.setLowerFrequency(_rangeSlider.lowerValue)
        _frequencyContainer.setUpperFrequency(_rangeSlider.upperValue)
        _graphViewController.setFrequencies(_rangeSlider.lowerValue,
            upperValue: _rangeSlider.upperValue)
    }

}