//
//  AxisContainer.swift
//  stethoMD
//
//  Created by Pierre Starkov on 15/06/15.
//  Copyright (c) 2015 Pierre Starkov. All rights reserved.
//

import Foundation
import UIKit

class AxisContainer: NSObject {
    static let sharedInstance = AxisContainer()
    
    var _bounds: CGRect!
    var _padSize: CGFloat = 25.0
    
    // size
    func setBounds(bounds: CGRect) {
        _bounds = bounds
    }
    func setPadSize(padSize: CGFloat) {
        _padSize = padSize
    }
    func getBounds() -> CGRect {
        return _bounds
    }
    func getPadSize() -> CGFloat {
        return _padSize
    }
    
    // preicise size
    func getX() -> CGFloat {
        return _padSize
    }
    func getY() -> CGFloat {
        return _padSize
    }
    func getWidth() -> CGFloat {
        return _bounds.width - 2 * _padSize
    }
    func getHeight() -> CGFloat {
        return _bounds.height - 2 * _padSize
    }
    func getOrigin() -> CGPoint {
        return CGPointMake(self.getX(), self.getY())
    }

    
}