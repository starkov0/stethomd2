//
//  MyMath.swift
//  stethoMD
//
//  Created by Pierre Starkov on 10/04/15.
//  Copyright (c) 2015 Pierre Starkov. All rights reserved.
//

import Foundation
import Accelerate

class MyLinspace: NSObject {
    static let sharedInstance = MyLinspace()
    
    func linspace(start: Int, end: Int, count: Int) -> Array<Int> {
        if count < 1 {
            return Array<Int>()
        }
        
        // delta
        var delta = Float(end-start)/Float(count-1)
        
        // linspace array
        var linspace = Array<Int>()
        for var i=0; i<count; i++ {
            linspace.append(start + Int(Float(i) * delta))
        }
        return linspace
    }
    
    func logspace(start: Int, end: Int, count: Int) -> Array<Int> {
        if count < 1 {
            return Array<Int>()
        }
        
        // delta
        var delta = log(Float(end-start))/Float(count-1)
        
        // logspace array
        var logspace = Array<Int>()
        for var i=0; i<count; i++ {
            logspace.append(start + Int(exp(Float(i) * delta)))
        }
        
        return logspace
    }
    
    private func audioWithIndexes(audio: [Float], indexes: [Int]) -> [Float] {
        var newAudio = Array<Float>()
        for var i=0; i<indexes.count; i++ {
            newAudio.append(audio[indexes[i]])
        }
        return newAudio
    }
    
    func linspaceAudio(audio: [Float], size: Int) -> [Float] {
        var indexes = self.linspace(0, end: audio.count-1, count: size)
        return self.audioWithIndexes(audio, indexes: indexes)
    }
    
    func logspaceAudio(audio: [Float], size: Int) -> [Float] {
        var indexes = self.logspace(0, end: audio.count-1, count: size)
        return self.audioWithIndexes(audio, indexes: indexes)
    }
}

