//
//  frequencyContainer.swift
//  stethoMD
//
//  Created by Pierre Starkov on 28/06/15.
//  Copyright (c) 2015 Pierre Starkov. All rights reserved.
//

import Foundation

class FrequencyContainer: NSObject {
    static let sharedInstance = FrequencyContainer()
    
    var _minFrequency = 20.0
    var _maxFrequency = 5000.0
    var _lowerFrequency = 20.0
    var _upperFrequency = 5000.0
    
    func getMinFrequency() -> Double {
        return _minFrequency
    }
    func getMaxFrequency() -> Double {
        return _maxFrequency
    }
    func getLowerFrequency() -> Double {
        return _lowerFrequency
    }
    func getUpperFrequency() -> Double {
        return _upperFrequency
    }
    
    func setLowerFrequency(lowerFrequency: Double) {
        _lowerFrequency = lowerFrequency
    }
    func setUpperFrequency(upperFrequency: Double) {
        _upperFrequency = upperFrequency
    }
    
}