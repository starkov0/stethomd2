//
//  AxisView.swift
//  stethoMD
//
//  Created by Pierre Starkov on 29/04/15.
//  Copyright (c) 2015 Pierre Starkov. All rights reserved.
//

import Foundation
import UIKit

class AxisView: UIView {
    let _axisContainer = AxisContainer.sharedInstance
    
    func getBounds() -> CGRect {
        return self.bounds
    }
    
    override func drawRect(rect: CGRect) {
        // left Y
        let leftY = UIBezierPath()
        leftY.moveToPoint(_axisContainer.getOrigin())
        leftY.addLineToPoint(_axisContainer.getOrigin())
        leftY.addLineToPoint(CGPoint(x: _axisContainer.getX(),
            y: _axisContainer.getY() + _axisContainer.getHeight()))
        leftY.stroke()
        
        // right Y
        let rightY = UIBezierPath()
        rightY.moveToPoint(CGPoint(x: _axisContainer.getX() + _axisContainer.getWidth(),
            y: _axisContainer.getY()))
        rightY.addLineToPoint(CGPoint(x: _axisContainer.getX() + _axisContainer.getWidth(),
            y: _axisContainer.getY()))
        rightY.addLineToPoint(CGPoint(x: _axisContainer.getX() + _axisContainer.getWidth(),
            y: _axisContainer.getY() + _axisContainer.getHeight()))
        rightY.stroke()
        
        // top X
        let topX = UIBezierPath()
        topX.moveToPoint(_axisContainer.getOrigin())
        topX.addLineToPoint(_axisContainer.getOrigin())
        topX.addLineToPoint(CGPoint(x: _axisContainer.getX() + _axisContainer.getWidth(),
            y: _axisContainer.getY()))
        topX.stroke()
        
        // bottom X
        let bottomX = UIBezierPath()
        bottomX.moveToPoint(CGPoint(x: _axisContainer.getX(),
            y: _axisContainer.getY() + _axisContainer.getHeight()))
        bottomX.addLineToPoint(CGPoint(x: _axisContainer.getX(),
            y: _axisContainer.getY() + _axisContainer.getHeight()))
        bottomX.addLineToPoint(CGPoint(x: _axisContainer.getX() + _axisContainer.getWidth(),
            y: _axisContainer.getY() + _axisContainer.getHeight()))
        bottomX.stroke()
    }
    
}