//
//  MyRegex.swift
//  stethoMD
//
//  Created by Pierre Starkov on 28/06/15.
//  Copyright (c) 2015 Pierre Starkov. All rights reserved.
//

import Foundation

class MyRegex: NSObject {
    static let sharedInstance = MyRegex()
    
    func matchesForRegexInText(regex: String!, text: String!) -> Bool {
        let regex = NSRegularExpression(pattern: regex,
            options: nil, error: nil)!
        let nsString = text as NSString
        let results = regex.matchesInString(text,
            options: nil,
            range: NSMakeRange(0, nsString.length))as! [NSTextCheckingResult]
        return results.count > 0
    }

}