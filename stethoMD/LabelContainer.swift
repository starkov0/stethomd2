//
//  LabelContainer.swift
//  stethoMD
//
//  Created by Pierre Starkov on 25/06/15.
//  Copyright (c) 2015 Pierre Starkov. All rights reserved.
//

import Foundation

class LabelContainer {
    static let sharedInstance = LabelContainer()
    
    var _labelX: [Int] = [Int]()
    var _labelY: [Int] = [Int]()
    
    func getLabelX() -> [Int] {
        return _labelX
    }
    func getLabelY() -> [Int] {
        return _labelY
    }
    
    func setLabelX(labelX: [Int]) {
        _labelX = labelX
    }
    func setLabelY(labelY: [Int]) {
        _labelY = labelY
    }
}