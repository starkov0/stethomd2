//
//  GraphView.swift
//  mobileMD
//
//  Created by Pierre Starkov on 09/04/15.
//  Copyright (c) 2015 Pierre Starkov. All rights reserved.
//

import Foundation
import UIKit

class GraphView : UIView {
    let _audioContainer = AudioContainer.sharedInstance
    let _myPathBuilder = MyPathBuilder.sharedInstance
    let _recordController = RecordController.sharedInstance
    
    // ************************************************ DRAW
    override func drawRect(rect: CGRect) {
        // get audio
        var points = _audioContainer.getAudio()
        
        // init arrays
        var positivePoints = [Float]()
        var negativePoints = [Float]()
        for var i=0; i<points.count; i++ {
            positivePoints.append(abs(points[i]))
            negativePoints.append(-abs(points[i]))
        }
        
        if _recordController.isRecording() {
            UIColor.blueColor().setStroke()
            UIColor.redColor().setFill()
        }

        // draw positive
        var myPositiveBezier = _myPathBuilder.buildTimePath(points: positivePoints)
        myPositiveBezier.fill()
        myPositiveBezier.stroke()
        
        // draw negative
        var myNegativeBezier = _myPathBuilder.buildTimePath(points: negativePoints)
        myNegativeBezier.fill()
        myNegativeBezier.stroke()
    }
}
