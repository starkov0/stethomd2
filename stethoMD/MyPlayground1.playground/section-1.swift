// Playground - noun: a place where people can play

import UIKit
import Foundation

class RunwayView : UIView {
    var floarArray: Array<Float>?
    
    ///Draws the "runway"
    override func drawRect(rect: CGRect) {
        
        var lala = Array<Float>()
        
        for var i=0; i<30; i++ {
            lala.append(Float(arc4random()) / Float(UINT32_MAX))
        }
        
        var myBezier = self.buildPath(points: lala, inBounds: self.bounds)
        UIColor.blackColor().setStroke()
        myBezier.stroke()

        var names = UIFont.familyNames()
        
        var myLabel = UILabel(frame: CGRectMake(250,250,200,20))
        myLabel.textColor = UIColor.blueColor()
        myLabel.font = UIFont(name: "Futura", size: 20.0)
        myLabel.text = "Oulala!"
        println(rect)
        self.addSubview(myLabel)
        
        
    }
    
    private func buildPath(#points: [Float], inBounds bounds: CGRect) -> UIBezierPath {
        if points.count < 2 {
            return UIBezierPath()
        }
        
        let maxValue = maxElement(points)
        
        if maxValue <= 0 {
            return UIBezierPath()
        }
        
        let path = UIBezierPath()
        path.moveToPoint(CGPoint(x: 0.0, y: bounds.height))
        
        for (index, point) in enumerate(points) {
            let xProgress = CGFloat(index) / CGFloat(points.count - 1)
            let normalizedValue = CGFloat(point) / CGFloat(maxValue)
            path.addLineToPoint(CGPoint(x: xProgress * bounds.width,
                y: bounds.height * (1.0 - normalizedValue)))
        }
        return path
    }
}


var names = UIFont.familyNames()
println(names)


var runway1 : RunwayView = RunwayView(frame: CGRectMake(0,0,500,500))
runway1.backgroundColor = UIColor.whiteColor()
runway1.setNeedsDisplay()
