//
//  MyPathBuilder.swift
//  stethoMD
//
//  Created by Pierre Starkov on 15/06/15.
//  Copyright (c) 2015 Pierre Starkov. All rights reserved.
//

import Foundation
import UIKit

class MyPathBuilder: NSObject {
    static let sharedInstance = MyPathBuilder()
    var _axisContainer = AxisContainer.sharedInstance
    
    func buildTimePath(#points: [Float]) -> UIBezierPath {
        // start path
        let path = UIBezierPath()
        path.moveToPoint(CGPoint(x: _axisContainer.getX(),
            y: _axisContainer.getY() + _axisContainer.getHeight() / 2))
        path.addLineToPoint(CGPoint(x: _axisContainer.getX(),
            y: _axisContainer.getY() + _axisContainer.getHeight() / 2))
        
        // fill path
        for (index, point) in enumerate(points) {
            let x = _axisContainer.getX() + CGFloat(index) / CGFloat(points.count - 1) * _axisContainer.getWidth()
            var y: CGFloat
            if point > 0.5 {
                y = _axisContainer.getY()
            } else if point < -0.5 {
                y = _axisContainer.getY() + _axisContainer.getHeight()
            } else {
                y = _axisContainer.getY() + _axisContainer.getHeight() * (0.5 - CGFloat(point))
            }
            path.addLineToPoint(CGPoint(x: x, y: y))
        }
        
        // end path
        path.addLineToPoint(CGPoint(x: _axisContainer.getX() + _axisContainer.getWidth(),
            y: _axisContainer.getY() + _axisContainer.getHeight() / 2))
        
        return path
    }
    
    func buildFFTPath(#points: [Float]) -> UIBezierPath {
        // start path
        let path = UIBezierPath()
        path.moveToPoint(CGPoint(x: _axisContainer.getX(),
            y: _axisContainer.getY() + _axisContainer.getHeight()))
        path.addLineToPoint(CGPoint(x: _axisContainer.getX(),
            y: _axisContainer.getY() + _axisContainer.getHeight()))

        // fill path
        for (index, point) in enumerate(points) {
            let x = _axisContainer.getX() + CGFloat(index) / CGFloat(points.count - 1) * _axisContainer.getWidth()
            var y: CGFloat
            if point > 1 {
                y = _axisContainer.getY()
            } else {
                y = _axisContainer.getY() + _axisContainer.getHeight() * (1 - CGFloat(point))
            }
            path.addLineToPoint(CGPoint(x: x, y: y))
        }
        
        // end path
        path.addLineToPoint(CGPoint(x: _axisContainer.getX() + _axisContainer.getWidth(),
            y: _axisContainer.getY() + _axisContainer.getHeight()))
        return path
    }

    
}